Overview
=========

This document is a walkthrough of the demo presented in this talk.

Requirements
------------
This demo uses the following software installed in the machine. Newer versions at the time of this talk would also work.

* Vagrant 2.2.0
* Docker Engine Client 18.09
* Docker Engine Server 18.09
* Virtualbox 5.2.18
* a clone of this repository
* A DockerHub account (Container registry) to push container images
* docker-compose version 1.23.1 

**Docker**

		→ docker version
		Client: Docker Engine - Community
		 Version:           18.09.0
		 API version:       1.39
		 Go version:        go1.10.4
		 Git commit:        4d60db4
		 Built:             Wed Nov  7 00:47:43 2018
		 OS/Arch:           darwin/amd64
		 Experimental:      false
		
		Server: Docker Engine - Community
		 Engine:
		  Version:          18.09.0
		  API version:      1.39 (minimum version 1.12)
		  Go version:       go1.10.4
		  Git commit:       4d60db4
		  Built:            Wed Nov  7 00:55:00 2018
		  OS/Arch:          linux/amd64
		  Experimental:     true


**Vagrant**

		→ vagrant version
		Installed Version: 2.2.0
		Latest Version: 2.2.1
		 
		To upgrade to the latest version, visit the downloads page and
		download and install the latest version of Vagrant from the URL
		below:
		
		  https://www.vagrantup.com/downloads.html
		
		If you're curious what changed in the latest release, view the
		CHANGELOG below:
		
		  https://github.com/hashicorp/vagrant/blob/v2.2.1/CHANGELOG.md
		  
**docker-compose**

	docker-compose version
	docker-compose version 1.23.1, build b02f1306
	docker-py version: 3.5.0
	CPython version: 3.6.6
	OpenSSL version: OpenSSL 1.1.0h  27 Mar 2018


The Flask application and Dockerfile
--------------
Navigate to the ```demo/app-code``` directory. This contains the sample Flask application, a requirements file and the Dockerfile manifest.

```Dockerfile```, ```app.py```, ```requirements.txt```


Building version 0 of the app and pushing the image to DockerHub
--------------

Login to DockerHub: ```docker login```

	docker login
	Authenticating with existing credentials...
	Login Succeeded


While still on the ```demo/app-code``` directory invoke the following command in the CLI:
**```docker build -t hadronp/alpine-flask:v0 .```** 

Substitute your DockerHub account name instead of **hadronp** eg. ```my_account/alpine-flask:v0```

	docker build -t hadronp/alpine-flask:v0 .
	Sending build context to Docker daemon  4.096kB
	Step 1/6 : FROM python:alpine3.7
	alpine3.7: Pulling from library/python
	c67f3896b22c: Already exists 
	8ea0547d7723: Pull complete 
	711fdf82ca0a: Pull complete 
	6002973f8486: Pull complete 
	08ec325072d0: Pull complete 
	Digest: sha256:8e083aa39117533772c0ecd4327403bb1acf9a9fc7ae14deac6fddeb8c28d2c4
	Status: Downloaded newer image for python:alpine3.7
	 ---> b9858a9efe1e
	Step 2/6 : COPY . /app
	 ---> bd100e61a5cb
	Step 3/6 : WORKDIR /app
	 ---> Running in 2d6e4f6a015b
	Removing intermediate container 2d6e4f6a015b
	 ---> 6aeafca5d304
	Step 4/6 : RUN pip install -r requirements.txt
	 ---> Running in 02fe15680891
	Collecting flask==1.0.2 (from -r requirements.txt (line 1))
	  Downloading https://files.pythonhosted.org/packages/7f/e7/08578774ed4536d3242b14dacb4696386634607af824ea997202cd0edb4b/Flask-1.0.2-py2.py3-none-any.whl (91kB)
	Collecting Werkzeug>=0.14 (from flask==1.0.2->-r requirements.txt (line 1))
	  Downloading https://files.pythonhosted.org/packages/20/c4/12e3e56473e52375aa29c4764e70d1b8f3efa6682bef8d0aae04fe335243/Werkzeug-0.14.1-py2.py3-none-any.whl (322kB)
	Collecting itsdangerous>=0.24 (from flask==1.0.2->-r requirements.txt (line 1))
	  Downloading https://files.pythonhosted.org/packages/76/ae/44b03b253d6fade317f32c24d100b3b35c2239807046a4c953c7b89fa49e/itsdangerous-1.1.0-py2.py3-none-any.whl
	Collecting click>=5.1 (from flask==1.0.2->-r requirements.txt (line 1))
	  Downloading https://files.pythonhosted.org/packages/fa/37/45185cb5abbc30d7257104c434fe0b07e5a195a6847506c074527aa599ec/Click-7.0-py2.py3-none-any.whl (81kB)
	Collecting Jinja2>=2.10 (from flask==1.0.2->-r requirements.txt (line 1))
	  Downloading https://files.pythonhosted.org/packages/7f/ff/ae64bacdfc95f27a016a7bed8e8686763ba4d277a78ca76f32659220a731/Jinja2-2.10-py2.py3-none-any.whl (126kB)
	Collecting MarkupSafe>=0.23 (from Jinja2>=2.10->flask==1.0.2->-r requirements.txt (line 1))
	  Downloading https://files.pythonhosted.org/packages/ac/7e/1b4c2e05809a4414ebce0892fe1e32c14ace86ca7d50c70f00979ca9b3a3/MarkupSafe-1.1.0.tar.gz
	Building wheels for collected packages: MarkupSafe
	  Running setup.py bdist_wheel for MarkupSafe: started
	  Running setup.py bdist_wheel for MarkupSafe: finished with status 'done'
	  Stored in directory: /root/.cache/pip/wheels/81/23/64/51895ea52825dc116a55f37043f49be0939bcf603de54e5cde
	Successfully built MarkupSafe
	Installing collected packages: Werkzeug, itsdangerous, click, MarkupSafe, Jinja2, flask
	Successfully installed Jinja2-2.10 MarkupSafe-1.1.0 Werkzeug-0.14.1 click-7.0 flask-1.0.2 itsdangerous-1.1.0
	Removing intermediate container 02fe15680891
	 ---> 38ee633f943c
	Step 5/6 : EXPOSE 5000
	 ---> Running in d9482c3d9366
	Removing intermediate container d9482c3d9366
	 ---> 372d0a4fe4f9
	Step 6/6 : CMD python ./app.py
	 ---> Running in eea1d28f06eb
	Removing intermediate container eea1d28f06eb
	 ---> c74382bedf20
	Successfully built c74382bedf20
	Successfully tagged hadronp/alpine-flask:v0

Show the newly built image in the terminal:

	docker image ls | grep alpine-flask
	hadronp/alpine-flask                       v0                  c74382bedf20        2 minutes ago       91.3MB

Push the container image to DockerHub: ```docker push hadronp/alpine-flask:v0```

	docker push hadronp/alpine-flask:v0
	The push refers to repository [docker.io/hadronp/alpine-flask]
	d06a3aaf817a: Pushed 
	8f83f81506cc: Pushed 
	9d47dcf94482: Layer already exists 
	1926d47f169d: Layer already exists 
	66bd78ef2de1: Layer already exists 
	55ad70ba131b: Layer already exists 
	ebf12965380b: Layer already exists 
	v0: digest: sha256:e6af025f2023fce5834618598162ec1c356753dc234a6fb0f2f1d608038962d8 size: 1786


Spawn a single container from the ```hadronp/alpine-flask:v0``` image
----------------

```docker run --detach --publish 5000:5000 hadronp/alpine-flask:v0```

	docker run --detach --publish 5000:5000 hadronp/alpine-flask:v0
	e3467d02e10d46a59538f7a8e7990f55e1a157b7b5a4a58d27cb54e5c8cf4d9e
	Ξ demo/app-code git:(master) ▶ docker ps
	CONTAINER ID        IMAGE                     COMMAND                  CREATED             STATUS              PORTS                    NAMES
	e3467d02e10d        hadronp/alpine-flask:v0   "/bin/sh -c 'python …"   27 seconds ago      Up 26 seconds       0.0.0.0:5000->5000/tcp   compassionate_snyder

Open a browser to view this impossibly awesome app for the first time. ```http://127.0.0.1:5000```


```docker-compose``` the first encounter with Infra as Code
----------------
 The ```docker-compose``` utility allows developers and systems operators to compose services and deployment in a YML format to allow for readability and collaboration.
 
 To add a reverse proxy in our deployed containers a Traefik [https://traefik.io]() container instance is spawned and attached to the Docker daemon. A test container **``whoami``** is also spawned to test Traefik.
 

**```docker-compose.yml```**

	version: '3'
	
	services:
	  reverse-proxy:
	    image: traefik # The official Traefik docker image
	    command: --api --docker # Enables the web UI and tells Traefik to listen to docker
	    ports:
	      - "80:80"     # The HTTP port
	      - "8080:8080" # The Web UI (enabled by --api)
	    volumes:
	      - /var/run/docker.sock:/var/run/docker.sock # So that Traefik can listen to the Docker events
	
	  whoami:
	    image: containous/whoami # A container that exposes an API to show its IP address
	    labels:
	      - "traefik.frontend.rule=Host:whoami.docker.localhost"
	
	  pytsada-flask:
	    image: hadronp/alpine-flask:v0
	    labels:
	      - "traefik.frontend.rule=Host:pytsada-flask.docker.localhost"

Run **```docker-compose up -d ```**

	docker-compose up -d  
	Creating network "demo_default" with the default driver
	Creating demo_pytsada-flask_1_dccf99d4ae41 ... done
	Creating demo_whoami_1_8042b3806a4d        ... done
	Creating demo_reverse-proxy_1_e470f048b7ea ... done

Invoking **```docker ps```** yields:

	docker ps
	CONTAINER ID        IMAGE                     COMMAND                  CREATED             STATUS              PORTS                                        NAMES
	4c7459a92d7f        containous/whoami         "/whoami"                24 seconds ago      Up 22 seconds       80/tcp                                       demo_whoami_1_27ea8d067551
	b8abfd82f212        traefik                   "/traefik --api --do…"   24 seconds ago      Up 21 seconds       0.0.0.0:80->80/tcp, 0.0.0.0:8080->8080/tcp   demo_reverse-proxy_1_ddffd84c1709
	ed8e3c2091ec        hadronp/alpine-flask:v0   "/bin/sh -c 'python …"   24 seconds ago      Up 22 seconds       5000/tcp                                     demo_pytsada-flask_1_302f1193b58c

The Traefik dashboard can be viewed via: **```http://localhost:8080/dashboard/```**. The **whoami** and the **pytsada-flask** can be viewed via **```whoami.docker.localhost```** and **```pytsada-flask.docker.localhost```** respectively.
	    


Launching more container instances (because the more the merrier)
-------

Launch 3  instances of the **```whoami```** service with the following command:
```docker-compose up -d --scale whoami=3```

	docker-compose up -d --scale whoami=3
	demo_pytsada-flask_1_302f1193b58c is up-to-date
	demo_reverse-proxy_1_ddffd84c1709 is up-to-date
	Starting demo_whoami_1_27ea8d067551 ... done
	Creating demo_whoami_2_7fe5c7aad37f ... done
	Creating demo_whoami_3_6e7a5f24e218 ... done

Examining shows that 2 more instances are spawned:

	docker ps | grep whoami
	16ad6d116e79        containous/whoami         "/whoami"                47 seconds ago      Up 45 seconds       80/tcp                                       demo_whoami_2_3932e71d13d5
	a3f2ee43bb2c        containous/whoami         "/whoami"                47 seconds ago      Up 45 seconds       80/tcp                                       demo_whoami_3_d719ef9fb8fb
	4c7459a92d7f        containous/whoami         "/whoami"                17 minutes ago      Up 17 minutes       80/tcp                                       demo_whoami_1_27ea8d067551


Traefik load-balances through the 3 container instances via a weighted round robin by default. 

Run the following command a couple of times and note the container hostname is different each time:

**``curl -H Host:whoami.docker.localhost http://127.0.0.1``**

	curl -H Host:whoami.docker.localhost http://127.0.0.1
	Hostname: 16ad6d116e79
	IP: 127.0.0.1
	IP: 172.22.0.6
	
	curl -H Host:whoami.docker.localhost http://127.0.0.1
	Hostname: a3f2ee43bb2c
	IP: 127.0.0.1
	IP: 172.22.0.5
	
	curl -H Host:whoami.docker.localhost http://127.0.0.1
	Hostname: 4c7459a92d7f
	IP: 127.0.0.1
	IP: 172.22.0.4

Now try scaling the **``pytsada-flask``** app: ```docker-compose up -d --scale pytsada-flask=2```

	docker-compose up -d --scale pytsada-flask=2
	Starting demo_pytsada-flask_1_302f1193b58c ... 
	Stopping and removing demo_whoami_2_3932e71d13d5 ... 
	Stopping and removing demo_whoami_2_3932e71d13d5 ... done
	Stopping and removing demo_whoami_3_d719ef9fb8fb ... done
	Creating demo_pytsada-flask_2_c788d5401891       ... done
	Starting demo_whoami_1_27ea8d067551              ... done
	
	docker ps | grep flask
	de373c6339e4        hadronp/alpine-flask:v0   "/bin/sh -c 'python …"   25 seconds ago      Up 23 seconds       5000/tcp                                     demo_pytsada-flask_2_f1ef49513a99
	ed8e3c2091ec        hadronp/alpine-flask:v0   "/bin/sh -c 'python …"   28 minutes ago      Up 28 minutes       5000/tcp                                     demo_pytsada-flask_1_302f1193b58c


Releasing new versions of our awesome Flask application
-------

In an immutable infrastructure paradigm, container images are **recreated** from scratch for each change in configuration and code. This ensures that each deployment in development, testing, staging, and production environments is consistent.

Assuming new code is pushed into version control, we then must rebuild the container image, tagging the artifact as appropriate.

	docker build -t hadronp/alpine-flask:v1 .
	Sending build context to Docker daemon  4.096kB
	Step 1/6 : FROM python:alpine3.7
	 ---> b9858a9efe1e
	Step 2/6 : COPY . /app
	 ---> 266852f05c68
	Step 3/6 : WORKDIR /app
	 ---> Running in f4df763269cf
	Removing intermediate container f4df763269cf
	 ---> 42139f22b903
	Step 4/6 : RUN pip install -r requirements.txt
	 ---> Running in ca4df3f0f47b
	Collecting flask==1.0.2 (from -r requirements.txt (line 1))
	  Downloading https://files.pythonhosted.org/packages/7f/e7/08578774ed4536d3242b14dacb4696386634607af824ea997202cd0edb4b/Flask-1.0.2-py2.py3-none-any.whl (91kB)
	Collecting click>=5.1 (from flask==1.0.2->-r requirements.txt (line 1))
	  Downloading https://files.pythonhosted.org/packages/fa/37/45185cb5abbc30d7257104c434fe0b07e5a195a6847506c074527aa599ec/Click-7.0-py2.py3-none-any.whl (81kB)
	Collecting itsdangerous>=0.24 (from flask==1.0.2->-r requirements.txt (line 1))
	  Downloading https://files.pythonhosted.org/packages/76/ae/44b03b253d6fade317f32c24d100b3b35c2239807046a4c953c7b89fa49e/itsdangerous-1.1.0-py2.py3-none-any.whl
	Collecting Jinja2>=2.10 (from flask==1.0.2->-r requirements.txt (line 1))
	  Downloading https://files.pythonhosted.org/packages/7f/ff/ae64bacdfc95f27a016a7bed8e8686763ba4d277a78ca76f32659220a731/Jinja2-2.10-py2.py3-none-any.whl (126kB)
	Collecting Werkzeug>=0.14 (from flask==1.0.2->-r requirements.txt (line 1))
	  Downloading https://files.pythonhosted.org/packages/20/c4/12e3e56473e52375aa29c4764e70d1b8f3efa6682bef8d0aae04fe335243/Werkzeug-0.14.1-py2.py3-none-any.whl (322kB)
	Collecting MarkupSafe>=0.23 (from Jinja2>=2.10->flask==1.0.2->-r requirements.txt (line 1))
	  Downloading https://files.pythonhosted.org/packages/ac/7e/1b4c2e05809a4414ebce0892fe1e32c14ace86ca7d50c70f00979ca9b3a3/MarkupSafe-1.1.0.tar.gz
	Building wheels for collected packages: MarkupSafe
	  Running setup.py bdist_wheel for MarkupSafe: started
	  Running setup.py bdist_wheel for MarkupSafe: finished with status 'done'
	  Stored in directory: /root/.cache/pip/wheels/81/23/64/51895ea52825dc116a55f37043f49be0939bcf603de54e5cde
	Successfully built MarkupSafe
	Installing collected packages: click, itsdangerous, MarkupSafe, Jinja2, Werkzeug, flask
	Successfully installed Jinja2-2.10 MarkupSafe-1.1.0 Werkzeug-0.14.1 click-7.0 flask-1.0.2 itsdangerous-1.1.0
	Removing intermediate container ca4df3f0f47b
	 ---> 3161ce0bbd3a
	Step 5/6 : EXPOSE 5000
	 ---> Running in e505ba351335
	Removing intermediate container e505ba351335
	 ---> b8942a7b0dd5
	Step 6/6 : CMD python ./app.py
	 ---> Running in d5f446e5eb57
	Removing intermediate container d5f446e5eb57
	 ---> 6610d3e21f09
	Successfully built 6610d3e21f09
	Successfully tagged hadronp/alpine-flask:v1

Push the new image version into DockerHub or any container registry of choice:

	docker push hadronp/alpine-flask:v1  
	The push refers to repository [docker.io/hadronp/alpine-flask]
	3f647ad6ddea: Pushed 
	2a704479cb24: Pushed 
	9d47dcf94482: Layer already exists 
	1926d47f169d: Layer already exists 
	66bd78ef2de1: Layer already exists 
	55ad70ba131b: Layer already exists 
	ebf12965380b: Layer already exists 
	v1: digest: sha256:4661a83984acb4d27517f3feda3ef1b8190cd95ffb854c9a29c84fa4fbb0453e size: 1786


Edit the **``docker-compose.yml``** file to use the new version of the app as per: **```hadronp/alpine-flask:v1```**:

	pytsada-flask:
	    image: hadronp/alpine-flask:v1
	    labels:
	      - "traefik.frontend.rule=Host:pytsada-flask.docker.localhost"

Rerun **``docker-compose up -d --scale pytsada-flask=5``** to deploy this image and set the running instances to 5:

	docker-compose up -d --scale pytsada-flask=5
	Recreating demo_pytsada-flask_1_302f1193b58c ... 
	demo_reverse-proxy_1_ddffd84c1709 is up-to-date
	Recreating demo_pytsada-flask_1_302f1193b58c ... done
	Creating demo_pytsada-flask_2_ab62d3b9be65   ... done
	Creating demo_pytsada-flask_3_76a74b115f1f   ... done
	Creating demo_pytsada-flask_4_f9acbd8fa1ff   ... done
	Creating demo_pytsada-flask_5_5ab2501e30b0   ... done

A closer examination shows that we have deployed 5 container instances of the new version of the Flask application:

	docker ps | grep flask                      
	12ead9aac5e1        hadronp/alpine-flask:v1   "/bin/sh -c 'python …"   12 seconds ago      Up 8 seconds        5000/tcp                                     demo_pytsada-flask_2_63dec689fb34
	d9190cf8d705        hadronp/alpine-flask:v1   "/bin/sh -c 'python …"   12 seconds ago      Up 8 seconds        5000/tcp                                     demo_pytsada-flask_4_a69afc2da83b
	9df2f06440b5        hadronp/alpine-flask:v1   "/bin/sh -c 'python …"   12 seconds ago      Up 7 seconds        5000/tcp                                     demo_pytsada-flask_5_ccf7ec64acfd
	a3f79965fcd2        hadronp/alpine-flask:v1   "/bin/sh -c 'python …"   12 seconds ago      Up 7 seconds        5000/tcp                                     demo_pytsada-flask_3_ce02757c32a0
	72538ac6dc91        hadronp/alpine-flask:v1   "/bin/sh -c 'python …"   15 seconds ago      Up 12 seconds       5000/tcp                                     demo_pytsada-flask_1_302f1193b58c


While we have conveniently deployed a new version of our app, we encountered a short downtime as instances are stopped and recreated to reflect the new state of our deployment. Unfortunately there are cases when even a **short downtime** is unacceptable, probably for SLA reasons.


Enter the Swarm
-------

Docker Swarm, the default container orchestration framework of Docker allows for HA (High Availability) deployments and rolling updates. For this demonstration we will use Vagrant to spawn 3 virtualized machines and build a 3-node Docker Swarm cluster.

Navigate to the **``infra-code/``** directory. The **``Vagrantfile``** defines in code how the virtual machines will be spawned. Running Vagrant would require Internet connection the first time in order to download the OS images and install packages.

Run **vagrant up** in the terminal, if this is the first time ```vagrant up``` is invoked it will take a while to spwan each virtual machine:

	vagrant up    
	Bringing machine 'docker-01' up with 'virtualbox' provider...
	Bringing machine 'docker-02' up with 'virtualbox' provider...
	Bringing machine 'docker-03' up with 'virtualbox' provider...
	==> docker-01: Importing base box 'bento/ubuntu-18.04'...
	==> docker-01: Matching MAC address for NAT networking...
	==> docker-01: Checking if box 'bento/ubuntu-18.04' is up to date...
	==> docker-01: Setting the name of the VM: infra-code_docker-01_1542874205965_18757
	==> docker-01: Clearing any previously set network interfaces...
	==> docker-01: Preparing network interfaces based on configuration...
	    docker-01: Adapter 1: nat
	    docker-01: Adapter 2: hostonly
	==> docker-01: Forwarding ports...
	    docker-01: 22 (guest) => 2222 (host) (adapter 1)
	==> docker-01: Running 'pre-boot' VM customizations...
	==> docker-01: Booting VM...
	==> docker-01: Waiting for machine to boot. This may take a few minutes...
	    docker-01: SSH address: 127.0.0.1:2222
	    docker-01: SSH username: vagrant
	    docker-01: SSH auth method: private key
	    docker-01: 
	    docker-01: Vagrant insecure key detected. Vagrant will automatically replace
	    docker-01: this with a newly generated keypair for better security.
	    docker-01: 
	    docker-01: Inserting generated public key within guest...
	    docker-01: Removing insecure key from the guest if it's present...
	    docker-01: Key inserted! Disconnecting and reconnecting using new SSH key...
	==> docker-01: Machine booted and ready!
	==> docker-01: Checking for guest additions in VM...
	==> docker-01: Setting hostname...
	==> docker-01: Configuring and enabling network interfaces...
	==> docker-01: Mounting shared folders...


Confirm that the virtual machines are online **```vagrant status```**:

	vagrant status
	Current machine states:
	
	docker-01                 running (virtualbox)
	docker-02                 running (virtualbox)
	docker-03                 running (virtualbox)
	
	This environment represents multiple VMs. The VMs are all listed
	above with their current state. For more information about a specific
	VM, run `vagrant status NAME`.


Open 3 Terminals and run **``vagrant ssh docker-01``**, **``vagrant ssh docker-02``**, **``vagrant ssh docker-03``** in each terminal, respectively:

	vagrant ssh docker-01
	Welcome to Ubuntu 18.04.1 LTS (GNU/Linux 4.15.0-29-generic x86_64)
	
	 * Documentation:  https://help.ubuntu.com
	 * Management:     https://landscape.canonical.com
	 * Support:        https://ubuntu.com/advantage
	
	  System information as of Thu Nov 22 08:33:58 UTC 2018
	
	  System load:  0.05              Users logged in:        0
	  Usage of /:   2.9% of 61.80GB   IP address for eth0:    10.0.2.15
	  Memory usage: 10%               IP address for eth1:    192.168.22.10
	  Swap usage:   0%                IP address for docker0: 172.17.0.1
	  Processes:    103
	
	
	129 packages can be updated.
	41 updates are security updates.
	
	
	vagrant@docker-01:~$ 

-

	vagrant ssh docker-02
	Welcome to Ubuntu 18.04.1 LTS (GNU/Linux 4.15.0-29-generic x86_64)
	
	 * Documentation:  https://help.ubuntu.com
	 * Management:     https://landscape.canonical.com
	 * Support:        https://ubuntu.com/advantage
	
	  System information as of Thu Nov 22 08:34:53 UTC 2018
	
	  System load:  0.0               Users logged in:        0
	  Usage of /:   2.9% of 61.80GB   IP address for eth0:    10.0.2.15
	  Memory usage: 9%                IP address for eth1:    192.168.22.11
	  Swap usage:   0%                IP address for docker0: 172.17.0.1
	  Processes:    95
	
	
	129 packages can be updated.
	41 updates are security updates.
	
-

	vagrant ssh docker-03
	Welcome to Ubuntu 18.04.1 LTS (GNU/Linux 4.15.0-29-generic x86_64)
	
	 * Documentation:  https://help.ubuntu.com
	 * Management:     https://landscape.canonical.com
	 * Support:        https://ubuntu.com/advantage
	
	  System information as of Thu Nov 22 08:35:52 UTC 2018
	
	  System load:  0.08              Users logged in:        0
	  Usage of /:   2.9% of 61.80GB   IP address for eth0:    10.0.2.15
	  Memory usage: 9%                IP address for eth1:    192.168.22.12
	  Swap usage:   0%                IP address for docker0: 172.17.0.1
	  Processes:    94
	
	
	126 packages can be updated.
	41 updates are security updates.
	
	
	vagrant@docker-03:~$ 


Run **``sudo su``** on all virtual machines, check and confirm that Docker is installed:

	vagrant@docker-03:~$ sudo su
	root@docker-03:/home/vagrant# docker version
	Client:
	 Version:           18.09.0
	 API version:       1.39
	 Go version:        go1.10.4
	 Git commit:        4d60db4
	 Built:             Wed Nov  7 00:49:01 2018
	 OS/Arch:           linux/amd64
	 Experimental:      false
	
	Server: Docker Engine - Community
	 Engine:
	  Version:          18.09.0
	  API version:      1.39 (minimum version 1.12)
	  Go version:       go1.10.4
	  Git commit:       4d60db4
	  Built:            Wed Nov  7 00:16:44 2018
	  OS/Arch:          linux/amd64
	  Experimental:     false
	root@docker-03:/home/vagrant# 


On **``docker-01``** run **``docker swarm init --advertise-addr=192.168.22.10``** to initialize the cluster and make **``docker-01``** as the manager node:

	root@docker-01:/home/vagrant# docker swarm init --advertise-addr=192.168.22.10
	Swarm initialized: current node (rrxn6b5tsqqs0wlug58psz51w) is now a manager.
	
	To add a worker to this swarm, run the following command:
	
	    docker swarm join --token SWMTKN-1-0gjc2ebonh5lae04cq7a0gkhrat5cbttybgimgxjupvsc40lml-476510h0zgmbsydpwymglnvv8 192.168.22.10:2377
	
	To add a manager to this swarm, run 'docker swarm join-token manager' and follow the instructions.
	
	root@docker-01:/home/vagrant# 

Take note of the join token generated by ``swarm init`` in this case: 
**``docker swarm join --token SWMTKN-1-0gjc2ebonh5lae04cq7a0gkhrat5cbttybgimgxjupvsc40lml-476510h0zgmbsydpwymglnvv8 192.168.22.10:2377``**

SSH into the other nodes **``docker-02``** and **``docker-03``**. 

Elevate to root user and copy paste the generated string to add these nodes as workers in the swarm cluster:

	root@docker-02:/home/vagrant# docker swarm join --token SWMTKN-1-0gjc2ebonh5lae04cq7a0gkhrat5cbttybgimgxjupvsc40lml-476510h0zgmbsydpwymglnvv8 192.168.22.10:2377
	This node joined a swarm as a worker.

-

	root@docker-03:/home/vagrant# docker swarm join --token SWMTKN-1-0gjc2ebonh5lae04cq7a0gkhrat5cbttybgimgxjupvsc40lml-476510h0zgmbsydpwymglnvv8 192.168.22.10:2377
	This node joined a swarm as a worker.
	

In **``docker-01``** run: ``docker node ls`` to examine the nodes in the cluster:

	root@docker-01:/home/vagrant# docker node ls
	ID                            HOSTNAME            STATUS              AVAILABILITY        MANAGER STATUS      ENGINE VERSION
	rrxn6b5tsqqs0wlug58psz51w *   docker-01           Ready               Active              Leader              18.09.0
	scz7a6u133sz4tgpxgjv1g5ml     docker-02           Ready               Active                                  18.09.0
	qftgb43prhaljetrthcvdy040     docker-03           Ready               Active                                  18.09.0	

We now have a working 3-node Docker Swarm cluster.


Portainer: for those who still like a fancy UI and ClickOps 
-------
[https://portainer.io/]()

Portainer is an open-source lightweight management UI which allows you to easily manage your Docker hosts or Swarm clusters.

To deploy Portainer in a Docker Swarm, first create a docker data volume for Portainer to mount and use, run the following command on the manager node **``docker-01``**:

``docker volume create portainer_data``

	root@docker-01:/home/vagrant# docker volume create portainer_data
	portainer_data

Display and confirm the volume created:
	
	root@docker-01:/home/vagrant# docker volume ls
	DRIVER              VOLUME NAME
	local               portainer_data

Create a service in the cluster for Portainer, run:

	docker service create \
	--name portainer \
	--publish 9000:9000 \
	--replicas=1 \
	--constraint 'node.role == manager' \
	--mount type=bind,src=//var/run/docker.sock,dst=/var/run/docker.sock \
	--mount type=volume,src=portainer_data,dst=/data \
	portainer/portainer \
	-H unix:///var/run/docker.sock
	
The service created will be as follows:

	root@docker-01:/home/vagrant# docker service ls
	ID                  NAME                MODE                REPLICAS            IMAGE                        PORTS
	ylexf2pahl6a        portainer           replicated          1/1                 portainer/portainer:latest   *:9000->9000/tcp

Login the Portainer console at ``http://192.168.22.10:9000``. You will be asked to set an admin password the first time you browse the link.


Recreating ```Traefik```, ```whoami``` and ```pytsada-flask``` services.
-------

All the terminal commands that follow must be run on the manager node (**``docker-01``**):

First, create an overlay network named **``traefik-net``**, this will be the managed network where we will deploy our services:

	root@docker-01:/home/vagrant# docker network create --driver=overlay traefik-net
	qp4k00w2pmlo9dmwb8mhg1amb

List the docker networks in the cluster:

	root@docker-01:/home/vagrant# docker network ls
	NETWORK ID          NAME                DRIVER              SCOPE
	9e41ecb1b839        bridge              bridge              local
	2a3a7b7ccdf2        docker_gwbridge     bridge              local
	47826adeb640        host                host                local
	9zn4njx4kvu7        ingress             overlay             swarm
	69dc4ed0e69a        none                null                local
	qp4k00w2pmlo        traefik-net         overlay             swarm

Create the **``Traefik``** service:

	docker service create \
	    --name traefik \
	    --constraint=node.role==manager \
	    --publish 80:80 \
	    --publish 8080:8080 \
	    --mount type=bind,source=/var/run/docker.sock,target=/var/run/docker.sock \
	    --network traefik-net \
	    traefik \
	    --docker \
	    --docker.swarmmode \
	    --docker.domain=traefik \
	    --docker.watch \
	    --web

Create the **``whoami``** service:

	docker service create \
	    --name whoami \
	    --label traefik.port=80 \
	    --label traefik.frontend.rule=Host:whoami.docker.localhost \
	    --network traefik-net \
	    containous/whoami

Lastly, create the service for **``pytsada-flask``**:

	docker service create \
	    --name pytsada-flask \
	    --label traefik.port=5000 \
	    --label traefik.frontend.rule=Host:pytsada-flask.docker.localhost \
	    --network traefik-net \
	    hadronp/alpine-flask:v0
	    
List all the services we have deployed so far:

	root@docker-01:/home/vagrant# docker service ls
	ID                  NAME                MODE                REPLICAS            IMAGE                        PORTS
	ylexf2pahl6a        portainer           replicated          1/1                 portainer/portainer:latest   *:9000->9000/tcp
	kqcx1siqnr99        pytsada-flask       replicated          1/1                 hadronp/alpine-flask:v0      
	nglc3sqb7q2g        traefik             replicated          1/1                 traefik:latest               *:80->80/tcp, *:8080->8080/tcp
	jbvukvf6zs22        whoami              replicated          1/1                 containous/whoami:latest     

To allow local name resolution edit the **``etc/hosts``** file in your machine and add the following line and save the file:

	192.168.22.10 whoami.docker.localhost pytsada-flask.docker.localhost

Test and confirm on the browser that the following services are reachable:

* Traefik: **``http://192.168.22.10:8080/dashboard/``**
* whoami: **``whoami.docker.localhost``**
* pytsada-flask: **``pytsada-flask.docker.localhost``**


Rolling Updates and zero downtime deployments
-------

We will have to redeploy the **``pytsada-flask``** service with additional parameters for replication, rolling update delay and parallel container updates.

First, destroy the existing  **``pytsada-flask``** service:


	root@docker-01:/home/vagrant# docker service rm pytsada-flask
	pytsada-flask

-

	root@docker-01:/home/vagrant# docker service ls
	ID                  NAME                MODE                REPLICAS            IMAGE                        PORTS
	ylexf2pahl6a        portainer           replicated          1/1                 portainer/portainer:latest   *:9000->9000/tcp
	nglc3sqb7q2g        traefik             replicated          1/1                 traefik:latest               *:80->80/tcp, *:8080->8080/tcp
	jbvukvf6zs22        whoami              replicated          1/1                 containous/whoami:latest     
	root@docker-01:/home/vagrant# 

Redeploy **``pytsada-flask``** with 8 replicas, a rolling update delay of 5 seconds updating 2 containers at a time:

	docker service create \
	    --name pytsada-flask \
	    --label traefik.port=5000 \
	    --label traefik.frontend.rule=Host:pytsada-flask.docker.localhost \
	    --network traefik-net \
	    --replicas 8 \
	    --update-delay 5s \
	    --update-parallelism 2 \
	    hadronp/alpine-flask:v0

The **``pytsada-flask``** service as deployed:

	root@docker-01:/home/vagrant# docker service ls
	ID                  NAME                MODE                REPLICAS            IMAGE                        PORTS
	ylexf2pahl6a        portainer           replicated          1/1                 portainer/portainer:latest   *:9000->9000/tcp
	l0dckwnipr0p        pytsada-flask       replicated          8/8                 hadronp/alpine-flask:v0      
	nglc3sqb7q2g        traefik             replicated          1/1                 traefik:latest               *:80->80/tcp, *:8080->8080/tcp
	jbvukvf6zs22        whoami              replicated          1/1                 containous/whoami:latest     

Furthermore, we can arbitrarily scale services on demand like:

	root@docker-01:/home/vagrant# docker service scale whoami=5
	whoami scaled to 5
	overall progress: 5 out of 5 tasks 
	1/5: running   [==================================================>] 
	2/5: running   [==================================================>] 
	3/5: running   [==================================================>] 
	4/5: running   [==================================================>] 
	5/5: running   [==================================================>] 
	verify: Service converged 	
-
	
	root@docker-01:/home/vagrant# docker service ls
	ID                  NAME                MODE                REPLICAS            IMAGE                        PORTS
	ylexf2pahl6a        portainer           replicated          1/1                 portainer/portainer:latest   *:9000->9000/tcp
	l0dckwnipr0p        pytsada-flask       replicated          8/8                 hadronp/alpine-flask:v0      
	nglc3sqb7q2g        traefik             replicated          1/1                 traefik:latest               *:80->80/tcp, *:8080->8080/tcp
	jbvukvf6zs22        whoami              replicated          5/5                 containous/whoami:latest     

Suppose we wish to deploy **``hadronp/alpine-flask:v1``**: we can do so by running:

**``docker service update --image hadronp/alpine-flask:v1 pytsada-flask``**

-


Examine where **``pytsada-flask``** is scheduled in our cluster:

	root@docker-01:/home/vagrant# docker service ps pytsada-flask
	ID                  NAME                  IMAGE                     NODE                DESIRED STATE       CURRENT STATE            ERROR               PORTS
	wxnvf8bvg9if        pytsada-flask.1       hadronp/alpine-flask:v1   docker-03           Running             Running 3 minutes ago                        
	1srql91fbet9         \_ pytsada-flask.1   hadronp/alpine-flask:v0   docker-03           Shutdown            Shutdown 3 minutes ago                       
	v8t57a909n8y        pytsada-flask.2       hadronp/alpine-flask:v1   docker-03           Running             Running 4 minutes ago                        
	qte661gd30ig         \_ pytsada-flask.2   hadronp/alpine-flask:v0   docker-02           Shutdown            Shutdown 4 minutes ago                       
	91egtlqy9c58        pytsada-flask.3       hadronp/alpine-flask:v1   docker-01           Running             Running 2 minutes ago                        
	n3xwynffp6q4         \_ pytsada-flask.3   hadronp/alpine-flask:v0   docker-01           Shutdown            Shutdown 2 minutes ago                       
	r6ueop1yls1f        pytsada-flask.4       hadronp/alpine-flask:v1   docker-03           Running             Running 3 minutes ago                        
	6jo4prmiuyug         \_ pytsada-flask.4   hadronp/alpine-flask:v0   docker-03           Shutdown            Shutdown 3 minutes ago                       
	ttpr0k935qxq        pytsada-flask.5       hadronp/alpine-flask:v1   docker-02           Running             Running 3 minutes ago                        
	5x7hul29p1th         \_ pytsada-flask.5   hadronp/alpine-flask:v0   docker-02           Shutdown            Shutdown 3 minutes ago                       
	bggf57a69f8h        pytsada-flask.6       hadronp/alpine-flask:v1   docker-02           Running             Running 4 minutes ago                        
	qpqce74bczn5         \_ pytsada-flask.6   hadronp/alpine-flask:v0   docker-03           Shutdown            Shutdown 4 minutes ago                       
	go02m3smfndu        pytsada-flask.7       hadronp/alpine-flask:v1   docker-02           Running             Running 2 minutes ago                        
	o4bl4fmsleky         \_ pytsada-flask.7   hadronp/alpine-flask:v0   docker-02           Shutdown            Shutdown 2 minutes ago                       
	n0bzu4fyeoif        pytsada-flask.8       hadronp/alpine-flask:v1   docker-01           Running             Running 3 minutes ago                        
	wcskqwvvfz9l         \_ pytsada-flask.8   hadronp/alpine-flask:v0   docker-01           Shutdown            Shutdown 3 minutes ago  

Or filter only running instances of the service:

	root@docker-01:/home/vagrant# docker service ps pytsada-flask -f "desired-state=running"
	ID                  NAME                IMAGE                     NODE                DESIRED STATE       CURRENT STATE           ERROR               PORTS
	wxnvf8bvg9if        pytsada-flask.1     hadronp/alpine-flask:v1   docker-03           Running             Running 6 minutes ago                       
	v8t57a909n8y        pytsada-flask.2     hadronp/alpine-flask:v1   docker-03           Running             Running 6 minutes ago                       
	91egtlqy9c58        pytsada-flask.3     hadronp/alpine-flask:v1   docker-01           Running             Running 5 minutes ago                       
	r6ueop1yls1f        pytsada-flask.4     hadronp/alpine-flask:v1   docker-03           Running             Running 5 minutes ago                       
	ttpr0k935qxq        pytsada-flask.5     hadronp/alpine-flask:v1   docker-02           Running             Running 5 minutes ago                       
	bggf57a69f8h        pytsada-flask.6     hadronp/alpine-flask:v1   docker-02           Running             Running 6 minutes ago                       
	go02m3smfndu        pytsada-flask.7     hadronp/alpine-flask:v1   docker-02           Running             Running 5 minutes ago                       
	n0bzu4fyeoif        pytsada-flask.8     hadronp/alpine-flask:v1   docker-01           Running             Running 5 minutes ago 


Furthermore, we can examine in detail the service deployed:

	root@docker-01:/home/vagrant# docker service inspect --pretty pytsada-flask
	
	ID:		l0dckwnipr0ps4kl4cxw1ccdq
	Name:		pytsada-flask
	Labels:
	 traefik.frontend.rule=Host:pytsada-flask.docker.localhost
	 traefik.port=5000
	Service Mode:	Replicated
	 Replicas:	8
	UpdateStatus:
	 State:		completed
	 Started:	10 minutes ago
	 Completed:	8 minutes ago
	 Message:	update completed
	Placement:
	UpdateConfig:
	 Parallelism:	2
	 Delay:		5s
	 On failure:	pause
	 Monitoring Period: 5s
	 Max failure ratio: 0
	 Update order:      stop-first
	RollbackConfig:
	 Parallelism:	1
	 On failure:	pause
	 Monitoring Period: 5s
	 Max failure ratio: 0
	 Rollback order:    stop-first
	ContainerSpec:
	 Image:		hadronp/alpine-flask:v1@sha256:4661a83984acb4d27517f3feda3ef1b8190cd95ffb854c9a29c84fa4fbb0453e
	 Init:		false
	Resources:
	Networks: traefik-net 
	Endpoint Mode:	vip
	root@docker-01:/home/vagrant# 

License
-------

MIT

Author Information
------------------

quantum34e@gmail.com
